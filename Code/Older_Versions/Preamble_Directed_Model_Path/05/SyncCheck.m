len=length(rx1.Data); % find length of dataset
index=idx.Data(len); % find index of correlation
symbol_offset = offset.Data(len); % find symbol offset
idx_value = idx.Data(len,1);
numFrames=20; % Set number of offset frames to overlay in the plot

% display index and symbol offset on the console
display([' ']);
display(['=====================================']);
msg = ['The correlation index is ' num2str(index)];
display([' ']);
display(['The correlation index is ' num2str(index)]);
display([' ']);
display(['The symbol offset is ' num2str(symbol_offset)]); 
display([' ']);
display(['The index is ' num2str(idx_value)]); 
display([' ']);
% plot the data
figure(4)
subplot(2,2,1)
plot(abs(rx2.Data(:,1,len-5))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Magnitude');
title('RX Signal vs Symbol');
grid on;

subplot(2,2,2)
plot(abs(corrOut.Data(:,1,len))) % plot the last correlation frame
xlabel('N Symbols');
ylabel('Magnitude');
title('Preamble Correlation');
grid on;

subplot(2,2,[3 4])
plot(abs(offset_removed.Data(len,:))) % plot the offset corrected frames
hold on;
if numFrames>1
    for j=1:numFrames
        plot(abs(offset_removed.Data(len-j,:)))
    end
end
xlabel('N Symbols');
ylabel('Magnitude');
title(['Offset Corrected RX Signal vs Symbols (Last ' num2str(numFrames) ' Frames)']);
grid on;
hold off
idx_vector = idx.Data(len-100:len,1);
figure(2)
subplot(2,2,1)
plot((idx_vector)); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('idx');
title('Frame Offset');
grid on;