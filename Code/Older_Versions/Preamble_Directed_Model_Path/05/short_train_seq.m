function [short_preamble,Xs] = short_train_seq(Nfft)
if nargin<1, Nfft=64; end
sqrt136 = sqrt(13/6)*(1+i);
Xs = zeros(1,53); % in the frequency domain for k=-26:26
% Xs([5 9 45 53 57]) = -sqrt136;
% Xs([13 17 21 25 41 49 61]) = sqrt136;
Xs([3 11 23 39 43 47 51]) = sqrt136;  Xs([7 15 19 31 35]) = -sqrt136;
xs = ifft([Xs(27:53) zeros(1,11) Xs(1:26)]);
short_preamble = [xs xs xs(1:Nfft/2)]; % in the time domain
if nargout==0
  subplot(211), stem([0:5*Nfft/2-1],real(short_preamble),'Markersize',5)
  set(gca,'XTick',[0:16:5*Nfft/2], 'fontsize',9)
end