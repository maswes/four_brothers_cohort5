close all;

len=length(rx1.Data); % find length of dataset
len2=length(rxIn.Data); % find length of dataset
index=idx.Data(len); % find index of correlation
len_preamble = length(long_preamble.Data);
index2 = long_preamble.Data(len_preamble,:);
%symbol_offset = offset.Data(len); % find symbol offset
idx_value = idx.Data(len,1);
numFrames=20; % Set number of offset frames to overlay in the plot

display(['The correlation index is ' num2str(index)]);
display([' ']);
% display(['The start of Long Preamble ' num2str(index2)]);
% display([' ']); 
figure(1)
subplot(3,1,1)
plot(real(tx400.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Real');
title('tx400');
grid on;
subplot(3,1,2)
plot(imag(tx400.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Imag');
title('tx400');
grid on;
subplot(3,1,3)
plot(abs(tx400.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Mag');
title('tx400');
grid on;

figure(2)
subplot(3,1,1)
plot(real(tx800.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Real');
title('tx800');
grid on;
subplot(3,1,2)
plot(imag(tx800.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Imag');
title('tx800');
grid on;
subplot(3,1,3)
plot(abs(tx800.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Mag');
title('tx800');
grid on;





figure(3)
subplot(3,1,1)
plot(real(rx1.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Real');
title('rx1');
grid on;
subplot(3,1,2)
plot(imag(rx1.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Imag');
title('rx1');
grid on;
subplot(3,1,3)
plot(abs(rx1.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Mag');
title('rx1');
grid on;


figure(4)
subplot(3,1,1)
plot(real(rx2.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Real');
title('rx2');
grid on;
subplot(3,1,2)
plot(imag(rx2.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Imag');
title('rx2');
grid on;
subplot(3,1,3)
plot(abs(rx2.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Mag');
title('rx2');
grid on;

figure(5)
subplot(3,1,1)
plot(real(offset_removed.Data(len,:))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Real');
title('offset_removed');
grid on;
subplot(3,1,2)
plot(imag(offset_removed.Data(len,:))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Imag');
title('offset_removed');
grid on;
subplot(3,1,3)
plot(abs(offset_removed.Data(len,:))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Mag');
title('offset_removed');
grid on;

figure(6)
subplot(2,1,1)
plot(real(corrOutReal.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Real');
title('CorrOutReal');
grid on;
subplot(2,1,2)
plot(imag(corrOutIm.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Imag');
title('CorrOutReal');
grid on;

figure(7)
subplot(2,1,1)
plot(real(corrOutReRe.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Real');
title('CorrOutReRe');
grid on;
subplot(2,1,2)
plot(imag(corrOutReRe.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Imag');
title('CorrOutReRe');
grid on;

figure(8)
subplot(2,1,1)
plot(real(corrOutImIm.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Real');
title('CorrOutImIn');
grid on;
subplot(2,1,2)
plot(imag(corrOutImIm.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Imag');
title('CorrOutImIm');
grid on;

figure(9)
subplot(2,1,1)
plot(real(corrOutReIm.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Real');
title('CorrOutReIm');
grid on;
subplot(2,1,2)
plot(imag(corrOutReIm.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Imag');
title('CorrOutReIm');
grid on;

figure(10)
subplot(2,1,1)
plot(real(corrOutImRe.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Real');
title('CorrOutImRe');
grid on;
subplot(2,1,2)
plot(imag(corrOutImRe.Data(:,1,len))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Imag');
title('CorrOutImRe');
grid on;

% figure(11)
% subplot(3,1,1)
% plot(real(IFFT_TX.Data(:,1,12759))); % plot the last rx signal frame
% xlabel('N Symbols');
% ylabel('Real');
% title('IFFT_TX');
% grid on;
% subplot(3,1,2)
% plot(imag(IFFT_TX.Data(:,1,12759))); % plot the last rx signal frame
% xlabel('N Symbols');
% ylabel('Imag');
% title('IFFT_TX');
% grid on;
% subplot(3,1,3)
% plot(abs(IFFT_TX.Data(:,1,12759))); % plot the last rx signal frame
% xlabel('N Symbols');
% ylabel('Mag');
% title('IFFT_TX');
% grid on;




figure(13)
subplot(2,1,1)
plot(real(corrD.Data(len,:))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Real');
title('CorrD');
grid on;
subplot(2,1,2)
plot(imag(corrD.Data(len,:))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Imag');
title('CorrD');
grid on;


figure(14)
subplot(3,1,1)
plot(real(long_preamble.Data(len,:))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Real');
title('Long Preamble');
grid on;
subplot(3,1,2)
plot(imag(long_preamble.Data(len,:))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Imag');
title('Long Preamble');
grid on;
subplot(3,1,3)
plot(abs(long_preamble.Data(len,:))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Mag');
title('Long Preamble');
grid on;

figure(15)
subplot(3,1,1)
plot(real(long_preamble_local.Data(len,:))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Real');
title('Local Long Preamble');
grid on;
subplot(3,1,2)
plot(imag(long_preamble_local.Data(len,:))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Imag');
title('Local Long Preamble');
grid on;
subplot(3,1,3)
plot(abs(long_preamble_local.Data(len,:))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Mag');
title('Local Long Preamble');
grid on;


figure(20)
subplot(3,1,1)
plot(real(rxIn.Data(:,len2))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Real');
title('Channel Estimator Input');
grid on;
subplot(3,1,2)
plot(imag(rxIn.Data(:,len2))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Imag');
title('Channel Estimator Input');
grid on;
subplot(3,1,3)
plot(abs(rxIn.Data(:,len2))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Mag');
title('Channel Estimator Input');
grid on;

figure(21)
subplot(3,1,1)
plot(real(rxIn_ch_est.Data(len2,:))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Real');
title('Channel Estimator Output');
grid on;
subplot(3,1,2)
plot(imag(rxIn_ch_est.Data(len2,:))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Imag');
title('Channel Estimator Output');
grid on;
subplot(3,1,3)
plot(abs(rxIn_ch_est.Data(len2,:))); % plot the last rx signal frame
xlabel('N Symbols');
ylabel('Mag');
title('Channel Estimator Output');
grid on;
